<?php
namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnLeadRule extends Rule
{
	public $name = 'ownDealRule';

	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['deal']) ? $params['deal']->owner == $user : false;
		}
		return false;
	}
}

