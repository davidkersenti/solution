<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "testField".
 *
 * @property integer $id
 * @property integer $req
 * @property integer $notreq
 */
class TestField extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'testField';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'req'], 'required'],
            [['id', 'req', 'notreq'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'req' => 'Req',
            'notreq' => 'Notreq',
        ];
    }
}
