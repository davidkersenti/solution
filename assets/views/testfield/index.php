<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TestfieldSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Test Fields';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-field-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Test Field', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'req',
            'notreq',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
