<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TestField */

$this->title = 'Create Test Field';
$this->params['breadcrumbs'][] = ['label' => 'Test Fields', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="test-field-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
